# -*- coding: utf-8 -*-
# Copyright 2016 Jonas Fourquier
# based on audiofeed by Joe Wreschnig
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation

import cPickle as pickle
import os, sys, threading, time, webbrowser

from gi.repository import Gtk, GLib, Pango, Gdk

import quodlibet
from quodlibet import config
from quodlibet import formats
from quodlibet import qltk
from quodlibet import util

from quodlibet.browsers import Browser
from quodlibet.formats import AudioFile
from quodlibet.formats.remote import RemoteFile
from quodlibet.qltk.downloader import DownloadWindow
from quodlibet.qltk.getstring import GetStringDialog
from quodlibet.qltk.msg import ErrorMessage
from quodlibet.qltk.songsmenu import SongsMenu
from quodlibet.qltk.views import AllTreeView
from quodlibet.qltk import Icons
from quodlibet.util import connect_obj
from quodlibet.util.path import get_home_dir
from quodlibet.qltk.x import ScrolledWindow, Align, Button, MenuItem


PODCASTS = os.path.join(quodlibet.get_user_dir(), "podcasts")
DND_URI_LIST, DND_MOZ_URL = range(2)

sys.modules["quodlibet.fake.browsers.podcasts"] = sys.modules[__name__]

class InvalidFeed(ValueError):
    pass


class Feed(list):
    def __init__(self, uri):
        self.name = _("Unknown")
        self.uri = uri
        self.changed = False
        self.website = ""
        self.__lastgot = 0

    def get_age(self):
        return time.time() - self.__lastgot

    @staticmethod
    def __fill_af(feed, af):
        try:
            af["title"] = feed.title or _("Unknown")
        except:
            af["title"] = _("Unknown")
        try:
            af["date"] = "%04d-%02d-%02d" % feed.modified_parsed[:3]
        except (AttributeError, TypeError):
            pass

        for songkey, feedkey in [
                ("website", "link"),
                ("description", "tagline"),
                ("language", "language"),
                ("copyright", "copyright"),
                ("organization", "publisher"),
                ("license", "license")]:
            try:
                value = getattr(feed, feedkey)
            except:
                pass
            else:
                if value and value not in af.list(songkey):
                    af.add(songkey, value)

        try:
            author = feed.author_detail
        except AttributeError:
            try:
                author = feed.author
            except AttributeError:
                pass
            else:
                if author and author not in af.list("artist"):
                    af.add('artist', author)
        else:
            try:
                if author.email and author.email not in af.list("contact"):
                    af.add("contact", author.email)
            except AttributeError:
                pass
            try:
                if author.name and author.name not in af.list("artist"):
                    af.add("artist", author.name)
            except AttributeError:
                pass

        try:
            values = feed.contributors
        except AttributeError:
            pass
        else:
            for value in values:
                try:
                    value = value.name
                except AttributeError:
                    pass
                else:
                    if value and value not in af.list("performer"):
                        af.add("performer", value)

        try:
            af["~#length"] = util.parse_time(feed.itunes_duration)
        except (AttributeError, ValueError):
            pass

        try:
            values = dict(feed.categories).values()
        except AttributeError:
            pass
        else:
            for value in values:
                if value and value not in af.list("genre"):
                    af.add("genre", value)

    def parse(self):
        try:
            doc = feedparser.parse(self.uri)
        except:
            return False

        try:
            album = doc.channel.title
        except AttributeError:
            return False

        if album:
            self.name = album
        else:
            self.name = _("Unknown")

        defaults = AudioFile({"feed": self.uri})
        try:
            self.__fill_af(doc.channel, defaults)
        except:
            return False

        entries = []
        uris = set()
        for entry in doc.entries:
            try:
                for enclosure in entry.enclosures:
                    try:
                        if ("audio" in enclosure.type or
                                "ogg" in enclosure.type or
                                formats.filter(enclosure.url)):
                            uri = enclosure.url.encode('ascii', 'replace')
                            try:
                                size = enclosure.length
                            except AttributeError:
                                size = 0
                            entries.append((uri, entry, size))
                            uris.add(uri)
                            break
                    except AttributeError:
                        pass
            except AttributeError:
                pass

        for entry in list(self):
            if entry["~uri"] not in uris:
                self.remove(entry)
            else:
                uris.remove(entry["~uri"])

        entries.reverse()
        for uri, entry, size in entries:
            if uri in uris:
                song = RemoteFile(uri)
                song["~#size"] = size
                song.fill_metadata = False
                song.update(defaults)
                song["album"] = self.name
                try:
                    self.__fill_af(entry, song)
                except:
                    pass
                else:
                    self.insert(0, song)
        self.__lastgot = time.time()
        return bool(uris)


class AddPodcastDialog(GetStringDialog):
    def __init__(self, parent):
        super(AddPodcastDialog, self).__init__(
            qltk.get_top_parent(parent), _("New Podcast"),
            _("Enter the location of an audio feed:"),
            button_label=_("_Add"), button_icon=Icons.LIST_ADD)

    def run(self):
        uri = super(AddPodcastDialog, self).run()
        if uri:
            return Feed(uri.encode('ascii', 'replace'))
        else:
            return None


class VisibilityAll(object):
    def __init__(self):
        self.name = _('All')

    def filter(self,songs):
        return songs


class VisibilityListen(object):
    def __init__(self):
        self.name = _('Listen')

    def filter(self,songs):
        return [song for song in songs if song.has_key('~#playcount') and song['~#playcount'] > 0]


class VisibilityNotListen(object):
    def __init__(self):
        self.name = _('Not listen')

    def filter(self,songs):
        return [song for song in songs if (not song.has_key('~#playcount')) or song['~#playcount'] == 0]


class Podcast(Browser):
    __visibility = Gtk.ListStore(object)
    __visibility.append(row=[VisibilityAll()])
    __visibility.append(row=[VisibilityListen()])
    __visibility.append(row=[VisibilityNotListen()])

    __feeds = Gtk.ListStore(object)

    headers = ("title artist performer ~people album date website language "
               "copyright organization license contact").split()

    name = _("Podcast")
    accelerated_name = _("_Podcast")
    keys = ["Podcast"]
    priority = 20
    uses_main_library = False

    __last_folder = get_home_dir()

    def pack(self, songpane):
        container = qltk.ConfigRHPaned("browsers", "podcast_pos", 0.4)
        self.show()
        container.pack1(self, True, False)
        container.pack2(songpane, True, False)
        return container

    def unpack(self, container, songpane):
        container.remove(songpane)
        container.remove(self)

    @staticmethod
    def cell_data(col, render, model, iter, data):
        if model[iter][0].changed:
            render.markup = "<b>%s</b>" % util.escape(model[iter][0].name)
        else:
            render.markup = util.escape(model[iter][0].name)
        render.set_property('markup', render.markup)

    @classmethod
    def changed(self, feeds):
        for row in self.__feeds:
            if row[0] in feeds:
                row[0].changed = True
                row[0] = row[0]
        Podcast.write()

    @classmethod
    def write(self):
        feeds = [row[0] for row in self.__feeds]
        with open(PODCASTS, "wb") as f:
            pickle.dump(feeds, f, pickle.HIGHEST_PROTOCOL)

    @classmethod
    def init(self, library):
        uris = set()
        try:
            with open(PODCASTS, "rb") as fileobj:
                feeds = pickle.load(fileobj)
        except (pickle.PickleError, EnvironmentError, EOFError):
            pass
        else:
            for feed in feeds:
                self.__feeds.append(row=[feed])
        GLib.idle_add(self.__do_check)

    @classmethod
    def __do_check(self):
        thread = threading.Thread(target=self.__check, args=())
        thread.setDaemon(True)
        thread.start()

    @classmethod
    def __check(self):
        for row in self.__feeds:
            feed = row[0]
            if feed.get_age() < 2 * 60 * 60:
                continue
            elif feed.parse():
                feed.changed = True
                row[0] = feed
        self.write()
        GLib.timeout_add(60 * 60 * 1000, self.__do_check)

    def Menu(self, songs, library, items):
        item_website = qltk.MenuItem(_(u"_Website"), 'applications-internet')
        if len(songs) == 1:
            item_download = qltk.MenuItem(_(u"_Download…"), 'document-save-as')
            item_download.connect('activate', self.__download, songs[0]("~uri"))
            item_download.set_sensitive(not songs[0].is_file)
            item_website.connect('activate', self.__website, songs[0]("website"))
            item_website.set_sensitive(not songs[0].is_file)
        else:
            songs = filter(lambda s: not s.is_file, songs)
            uris = [song("~uri") for song in songs]
            item_download = qltk.MenuItem(_(u"_Download…"), 'document-save-as')
            item_download.connect('activate', self.__download_many, uris)
            item_download.set_sensitive(bool(songs))
            item_website.set_sensitive(False)

        items.append([item_website, item_download])
        item_listen = qltk.MenuItem(_(u"_Mark as listen"), 'audio-volume-high')
        item_listen.connect('activate', self.__mark_listen, songs)
        item_notisten = qltk.MenuItem(_(u"_Mark as not listen"), 'audio-volume-muted')
        item_notisten.connect('activate', self.__mark_notlisten, songs)
        items.append([item_listen, item_notisten])

        menu = SongsMenu(library, songs, items=items)
        return menu

    def __download_many(self, activator, sources):
        chooser = Gtk.FileChooserDialog(
            title=_("Download Files"), parent=qltk.get_top_parent(self),
            action=Gtk.FileChooserAction.CREATE_FOLDER)
        chooser.add_button(_("_Cancel"), Gtk.ResponseType.CANCEL)
        chooser.add_button(_("_Save"), Gtk.ResponseType.OK)
        chooser.set_current_folder(self.__last_folder)
        resp = chooser.run()
        if resp == Gtk.ResponseType.OK:
            target = chooser.get_filename()
            if target:
                type(self).__last_folder = os.path.dirname(target)
                for i, source in enumerate(sources):
                    base = os.path.basename(source)
                    if not base:
                        base = ("file%d" % i) + (
                            os.path.splitext(source)[1] or ".audio")
                    fulltarget = os.path.join(target, base)
                    DownloadWindow.download(source, fulltarget, self)
        chooser.destroy()

    def __download(self, activator, source):
        chooser = Gtk.FileChooserDialog(
            title=_("Download File"), parent=qltk.get_top_parent(self),
            action=Gtk.FileChooserAction.SAVE)
        chooser.add_button(_("_Cancel"), Gtk.ResponseType.CANCEL)
        chooser.add_button(_("_Save"), Gtk.ResponseType.OK)
        chooser.set_current_folder(self.__last_folder)
        name = os.path.basename(source)
        if name:
            chooser.set_current_name(name)
        resp = chooser.run()
        if resp == Gtk.ResponseType.OK:
            target = chooser.get_filename()
            if target:
                type(self).__last_folder = os.path.dirname(target)
                DownloadWindow.download(source, target, self)
        chooser.destroy()

    def __mark_listen(self, activator, songs):
        for song in songs :
            if not song.has_key('~#playcount') or song['~#playcount'] == 0:
                song['~#playcount'] = 1

    def __mark_notlisten(self, activator, songs):
        for song in songs :
            if song.has_key('~#playcount') and song['~#playcount'] != 0 :
                song['~#playcount'] = 0

    def __website(self, activator, website):
        util.website(website.split()[-1])#todo
        #~ webbrowser.open(website.split()[-1])#todo

    def __init__(self, library):
        super(Podcast, self).__init__(spacing=6)
        self.set_orientation(Gtk.Orientation.VERTICAL)

        self.__view_visibility = view_visibility = AllTreeView()
        self.__render = render = Gtk.CellRendererText()
        render.set_property('ellipsize', Pango.EllipsizeMode.END)
        col = Gtk.TreeViewColumn("Visibility", render)
        def cell_visibility(col, render, model, iter, data):
            render.markup = util.escape(model[iter][0].name)
            render.set_property('markup', render.markup)
        col.set_cell_data_func(render, cell_visibility)
        view_visibility.append_column(col)
        view_visibility.set_model(self.__visibility)
        view_visibility.set_rules_hint(True)
        view_visibility.set_headers_visible(False)
        try:
            select_visibility = config.get("browsers", "podcast.visibility")
        except:
            view_visibility.set_cursor(0)
        else:
            view_visibility.select_by_func(lambda r: r[0].name == select_visibility)
        swin = ScrolledWindow()
        swin.set_shadow_type(Gtk.ShadowType.IN)
        swin.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC)
        swin.add(view_visibility)
        swin.set_size_request(-1,100)
        self.pack_start(swin, False, True, 0)

        self.__view_podcast = view_podcast = AllTreeView()
        self.__render = render = Gtk.CellRendererText()
        render.set_property('ellipsize', Pango.EllipsizeMode.END)
        col = Gtk.TreeViewColumn("Podcast", render)
        col.set_cell_data_func(render, Podcast.cell_data)
        view_podcast.append_column(col)
        view_podcast.set_model(self.__feeds)
        view_podcast.set_rules_hint(True)
        view_podcast.set_headers_visible(False)
        try:
            select_feed = config.get("browsers", "podcast.feeds").split("\t")
        except:
            pass
        else:
            self.__view_podcast.select_by_func(lambda r: r[0].name in select_feed)
        swin = ScrolledWindow()
        swin.set_shadow_type(Gtk.ShadowType.IN)
        swin.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC)
        swin.add(view_podcast)
        self.pack_start(swin, True, True, 0)

        new = Button(_("_New podcast"), Icons.LIST_ADD, Gtk.IconSize.MENU)
        new.connect('clicked', self.__new_feed)
        view_visibility.get_selection().connect('changed', self.__changed)
        view_podcast.get_selection().connect('changed', self.__changed)
        view_podcast.get_selection().set_mode(Gtk.SelectionMode.MULTIPLE)
        view_podcast.connect('popup-menu', self.__popup_menu)

        view_visibility.connect('row-activated', self.__row_activated)
        view_podcast.connect('row-activated', self.__row_activated)

        targets = [
            ("text/uri-list", 0, DND_URI_LIST),
            ("text/x-moz-url", 0, DND_MOZ_URL)
        ]
        targets = [Gtk.TargetEntry.new(*t) for t in targets]

        view_podcast.drag_dest_set(Gtk.DestDefaults.ALL, targets, Gdk.DragAction.COPY)
        view_podcast.connect('drag-data-received', self.__drag_data_received)
        view_podcast.connect('drag-motion', self.__drag_motion)
        view_podcast.connect('drag-leave', self.__drag_leave)

        connect_obj(self, 'destroy', self.__save, view_podcast)

        self.pack_start(Align(new, left=3, bottom=3), False, True, 0)

        swin = ScrolledWindow()
        swin.set_shadow_type(Gtk.ShadowType.IN)
        swin.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC)

        for child in self.get_children():
            child.show_all()

    def __drag_motion(self, view, ctx, x, y, time):
        targets = [t.name() for t in ctx.list_targets()]
        if "text/x-quodlibet-songs" not in targets:
            view.get_parent().drag_highlight()
            return True
        return False

    def __drag_leave(self, view, ctx, time):
        view.get_parent().drag_unhighlight()

    def __drag_data_received(self, view, ctx, x, y, sel, tid, etime):
        view.emit_stop_by_name('drag-data-received')
        targets = [
            ("text/uri-list", 0, DND_URI_LIST),
            ("text/x-moz-url", 0, DND_MOZ_URL)
        ]
        targets = [Gtk.TargetEntry.new(*t) for t in targets]

        view.drag_dest_set(Gtk.DestDefaults.ALL, targets, Gdk.DragAction.COPY)
        if tid == DND_URI_LIST:
            uri = sel.get_uris()[0]
        elif tid == DND_MOZ_URL:
            uri = sel.data.decode('utf16', 'replace').split('\n')[0]
        else:
            ctx.finish(False, False, etime)
            return

        ctx.finish(True, False, etime)

        feed = Feed(uri.encode("ascii", "replace"))
        feed.changed = feed.parse()
        if feed:
            self.__feeds.append(row=[feed])
            Podcast.write()
        else:
            ErrorMessage(
                self, _("Unable to add feed"),
                _("%s could not be added. The server may be down, "
                  "or the location may not be an audio feed.") %
                util.bold(util.escape(feed.uri))).run()

    def __popup_menu(self, view):
        model, paths = view.get_selection().get_selected_rows()
        menu = Gtk.Menu()
        refresh = MenuItem(_("_Refresh"), Icons.VIEW_REFRESH)
        delete = MenuItem(_("_Delete"), Icons.EDIT_DELETE)

        connect_obj(refresh,
            'activate', self.__refresh, [model[p][0] for p in paths])
        connect_obj(delete,
            'activate', map, model.remove, map(model.get_iter, paths))

        menu.append(refresh)
        menu.append(delete)
        menu.show_all()
        menu.connect('selection-done', lambda m: m.destroy())

        # XXX: keep the menu arround
        self.__menu = menu

        return view.popup_menu(menu, 0, Gtk.get_current_event_time())

    def __save(self, view):
        Podcast.write()

    def __refresh(self, feeds):
        changed = filter(Feed.parse, feeds)
        Podcast.changed(changed)

    def __row_activated(self, treeview ,path, col_view):
        self.__changed(self.__view_podcast.get_selection())
        GLib.idle_add(self.unpause)

    def __changed(self, selection):
        model, paths = self.__view_visibility.get_selection().get_selected_rows()
        visibility_filter = model[paths][0].filter
        config.set("browsers", "podcast.visibility",model[paths][0].name)
        model, paths = self.__view_podcast.get_selection().get_selected_rows()
        if model and paths:
            songs = []
            for path in paths:
                model[path][0].changed = False
                songs.extend(model[path][0])
            self.songs_selected(visibility_filter(songs), True)
            config.set("browsers", "podcast.feeds",
                       "\t".join([model[path][0].name for path in paths]))

    def __new_feed(self, activator):
        feed = AddPodcastDialog(self).run()
        if feed is not None:
            feed.changed = feed.parse()
            if feed:
                self.__feeds.append(row=[feed])
                Podcast.write()
            else:
                ErrorMessage(
                    self, _("Unable to add feed"),
                    _("%s could not be added. The server may be down, "
                      "or the location may not be an audio feed.") %
                    util.bold(util.escape(feed.uri))).run()

    def restore(self):
        pass
        #~ try:
            #~ visibility = config.get("browsers", "podcast.visibility")
            #~ names = config.get("browsers", "podcast.feeds").split("\t")
        #~ except:
            #~ pass
        #~ else:
            #~ self.__view_visibility.select_by_func(lambda r: r[0].name == visibility)
            #~ self.__view_podcast.select_by_func(lambda r: r[0].name in names)

    def unpause(self):
        try:
            app.player.next()
        except AttributeError:
            app.player.paused = True

browsers = []
try:
    import feedparser
except ImportError:
    print_w(_("Could not import %s. Podcast browser disabled.")
            % "python-feedparser")
else:
    from quodlibet import app
    if not app.player or app.player.can_play_uri("http://"):
        browsers = [Podcast]
    else:
        print_w(_("The current audio backend does not support URLs, "
                  "Podcast browser disabled."))
