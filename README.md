# Podcasts browser for Quod Libet

This project is a podcasts browser for [Quod Libet](https://quodlibet.readthedocs.org/)
audio player.

![quodlibet_podcast](/uploads/9205b134c70eb17a69b5c066033072a5/quodlibet_podcast.png)

It is based on official audiofeed browser by Joe Wreschnig. The added
features are:

  * Import only listen or only not listen
  * Mark as listen / not listen in songmenu
  * Website link in songsmenu


## Install

Copy audiofeeds.py in ```/usr/lib/python2.7/dist-packages/quodlibet/browsers/```

Restart quodlibet


## Compatibility

This plugin has been tested and works with Quod Libet :
  * 3.6.2
  * 3.7.0


## Support

gitlab issues or irc (SnouF @ freenode)

